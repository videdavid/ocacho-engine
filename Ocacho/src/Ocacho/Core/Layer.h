/************************************************************************/
/* Base layer of Ocacho engine.
*
* Our engine is based on layers. This is the base class of them.
* Capable of handling events.
/************************************************************************/

#pragma once

#include "ogpch.h"
#include "Ocacho/Events/Event.h"
#include "Ocacho/Events/IEventListener.h"
 
namespace Ocacho
{
	class Layer : public EventListener
	{
		/*Name of the layer. Used for debuging purpouses*/
		PROTECTED(std::string, GET_INLINE, m_name, Name);

	public:
		Layer(const std::string& name = "Layer");
		virtual ~Layer();

		/**
		* Each time a layer is pushed, onAttach will be called
		* As init of a layer
		*/
		virtual void onAttach() {}

		/**
		* Each time a layer is popped, onDetach will be called
		* As shutdown of a layer
		*/
		virtual void onDetach() {}

		/**
		* Called each frame, if the layer is on the layers vector
		* As update of a layer
		*/
		virtual void onUpdate() {}

		/**
		* Called each render time, if the layer is on the layers vector
		* As render of a layer
		*/
		virtual void onRender() {}

		/***** EventListener Interface *****/
		virtual void onEvent(Event& event) override {}
	};
}

