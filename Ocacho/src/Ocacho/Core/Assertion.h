/************************************************************************/
/* Assertion system of Ocacho engine.
*
* This allow us to check conditions easily.
* As a debug mode, we can stop the game with a debug point.
* 
* Example -> OG_ENGINE_ASSERT_BREAK(5 == 4, "This is gonna fail");
/************************************************************************/

#pragma once

#ifdef OG_ENABLE_ASSERTS
	#define OG_GAME_ASSERT(x, ...) { if(!(x)) { OG_GAME_LOG_ERROR("Assertion Failed: {0}", __VA_ARGS__); } }
	#define OG_GAME_ASSERT_BREAK(x, ...) { if(!(x)) { OG_GAME_LOG_ERROR("Assertion Failed: {0}", __VA_ARGS__); OG_DEBUG_BREAK; } }
	#define OG_ENGINE_ASSERT(x, ...) { if(!(x)) { OG_ENGINE_LOG_ERROR("Assertion Failed: {0}", __VA_ARGS__); } }
	#define OG_ENGINE_ASSERT_BREAK(x, ...) { if(!(x)) { OG_ENGINE_LOG_ERROR("Assertion Failed: {0}", __VA_ARGS__); OG_DEBUG_BREAK; } }
	#
#else
	#define OG_ASSERT(x, ...)
	#define OG_ASSERT_BREAK(x, ...)
	#define OG_ENGINE_ASSERT(x, ...)
	#define OG_ENGINE_ASSERT_BREAK(x, ...)
#endif
