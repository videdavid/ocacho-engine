/************************************************************************/
/* This is the header file all the projects made by Ocacho should use
*  to include all the modules.
*
*  See an example in SandboxApp.
/************************************************************************/

#pragma once

#include "Ocacho/Core/Application.h"
#include "Ocacho/Log/Log.h"
#include "Ocacho/Core/Assertion.h"
#include "Ocacho/Macros/DefinitionMacros.h"
#include "Ocacho/Core/Layer.h"
#include "Ocacho/ImGui/ImGuiLayer.h"
#include "Ocacho/Core/EntryPoint.h"
#include "Ocacho/Input/Input.h"
#include "Ocacho/Input/KeyCodes.h"
#include "Ocacho/Input/MouseCodes.h"
#include "Ocacho/Profiling/ProfilingTimer.h"
