/************************************************************************/
/* Core operations of Ocacho engine.
*
* Macros, ifdefined, platform exclusive and different necessary operations.
/************************************************************************/

#pragma once

#if defined OG_PLATFORM_WINDOWS
	#define OG_DEBUG_BREAK  { __debugbreak(); }
#elif defined OG_PLATFORM_LINUX
	#include <signal.h>
	#define OG_DEBUG_BREAK { raise(SIGTRAP); } 
#else
	#error Unsupported platform
#endif

#define BIT(x) (1 << x)

/*Macro for binding function to events */
#define OG_BIND_EVENT_FN(fn) std::bind(&fn, this, std::placeholders::_1)
