/************************************************************************/
/* Base application of Ocacho engine.
*
* This class is the base of the each game. It will handle the main loop
* of the game, events and layer stack.
* Each game should derived from Application.
* To create a game the derived classes should implement ::CreateApplication
* returning the game. See an example in SandboxApp
*
/************************************************************************/

#pragma once

#include "ogpch.h"
#include "Window.h"
#include "Ocacho/Events/Event.h"
#include "Ocacho/Events/ApplicationEvent.h"
#include "LayerStack.h"
#include "Ocacho/ImGui/ImGuiLayer.h"

namespace Ocacho {

	class Application : public Singleton<Application>, public EventListener
	{
	private:
		/*The unique window of our application*/
		std::unique_ptr<Window> m_window;

		/*The predefined ImGui layer*/
		ImGuiLayer* m_imguiLayer;

		/*Whether our application is running or not*/
		bool m_running = true;

		/*Layer stack for handling game layers*/
		LayerStack m_layerStack;

	private:
		/**
		* Once we received the WindowClosedEvent this function will be called
		*/
		bool onWindowClosed(WindowCloseEvent& e);

	public:

		GENERATE_CLASS_METADATA(Application)

		Application();
		virtual ~Application();

		/**
		* Main loop of the game
		*/
		void run();

		/**
		* Push a game layer to the vector of layers of the application
		* This allows clients to be able to handle with layers
		*/
		inline void pushLayer(Layer* layer) { m_layerStack.pushLayer(layer); }

		/**
		* Push a game overlay to the vector of layers of the application
		* This allows clients to be able to handle with layers
		*/
		inline void pushOverlay(Layer* overlay) { m_layerStack.pushOverlay(overlay); }

		/**
		* Return the unique and current window of our application
		*/
		inline Window& getWindow() { return *m_window; }

		/*****EventListener Interface*****/
		virtual void onEvent(Event& e) override;

	};

	/**
	* Create an application of the current game
	* This is not a function from Application
	* This function should be implemented in each game to return the game. See Sandbox app
	* 
	* @return The created application
	*/
	Application* createApplication();

}