/************************************************************************/
/* This are the base object macros for Ocacho engine.
*
* This macros are used to create the basic structure of an object of Ocacho engine.
* 
* GENERATE_CLASS_METADATA should be called each time we derived from BaseObject.h 
* to generate the basic methods for each object.
/************************************************************************/

#pragma once

/*-----------------------------------------------------------------------------
BaseObject macros
-----------------------------------------------------------------------------*/

#define GENERATE_NAME_STATIC(...)\
	static std::string getClassNameStatic(){\
		static std::string className = std::string(# __VA_ARGS__);\
		return className;\
	};

#define GENERATE_NAME_VIRTUAL(...)\
	inline std::string getClassName() const override{\
		return __VA_ARGS__::getClassNameStatic();\
	};

#define GENERATE_CLASS_METADATA(...)\
	GENERATE_NAME_STATIC(__VA_ARGS__);\
	GENERATE_NAME_VIRTUAL(__VA_ARGS__);


/*-----------------------------------------------------------------------------
Super macro
-----------------------------------------------------------------------------*/

#define GENERATE_SUPER(...)\
	typedef __VA_ARGS__ super;
	

