/************************************************************************/
/* Stack for managing the layers of Ocacho engine
*
* Our engine is based on layers, we want a way to handle with the
* order of the overlays and layers for rendering.
* 
* An overlay is also a layer.
* Overlays will always be rendered after all the layers.
/************************************************************************/

#pragma once

#include "ogpch.h"
#include "Layer.h"

namespace Ocacho
{
	class LayerStack
	{
	private:
		/*Vector of the layers we are handling*/
		std::vector<Layer*> m_layers;

		/*Current index of the last inserted layer, excluding overlays*/
		unsigned int m_layerInsertIndex = 0;

	public:
		LayerStack();
		~LayerStack();

		/**
		* Push a layer to the vector of layers
		* This layer will be rendered the last between the layers
		*/
		void pushLayer(Layer* layer);

		/**
		* Push an overlay to the vector of layers
		* This overlay will be rendered the last
		*/
		void pushOverlay(Layer* overlay);

		/**
		* Pop a layer from the vector of layers
		*/
		void popLayer(Layer* layer);

		/**
		* Pop an overlay from the vector of layers
		*/
		void popOverlay(Layer* overlay);

		/*Overriding begin to be able to loop through the layer stack easily*/
		inline std::vector<Layer*>::iterator begin() { return m_layers.begin(); }

		/*Overriding end to be able to loop through the layer stack easily*/
		inline std::vector<Layer*>::iterator end() { return m_layers.end(); }
	};
}


