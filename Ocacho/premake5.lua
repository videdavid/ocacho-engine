project "Ocacho"
	kind "StaticLib"
	language "C++"
	cppdialect "C++17"
	staticruntime "on"

    targetdir ("%{wks.location}/bin/" .. outputdir .. "/%{prj.name}")
	objdir ("%{wks.location}/bin-int/" .. outputdir .. "/%{prj.name}")

	--Precompiled headers --

	pchheader "ogpch.h"
	pchsource "src/ogpch.cpp"

	files
	{
		"src/**.h",
		"src/**.cpp"
	}

	includedirs
	{
		"src",
		"vendor/spdlog/include",
		"%{IncludeDir.GLFW}",
		"%{IncludeDir.Glad}",
		"%{IncludeDir.ImGui}",
		"%{IncludeDir.glm}"
	}

	links 
	{ 
		"GLFW",
		"Glad",
		"ImGui"
	}

	defines
	{
		"OG_ENABLE_ASSERTS",
		"GLFW_INCLUDE_NONE", --no incluira ninguna libreria de glfw si hemos metido glad
		"_CRT_SECURE_NO_WARNINGS"
	}

	filter "system:linux"
		systemversion "latest"

		-- Add Linux-specific files
		files
		{
			--"%{prj.name}/src/Platform/Linux/**.h",
			--"%{prj.name}/src/Platform/Linux/**.cpp"
		}

		files
		{
			"%{prj.name}/src/Platform/Windows/**.h",
			"%{prj.name}/src/Platform/Windows/**.cpp",
		}

		links
		{
			"Xrandr",
			"Xi",
			"GLEW",
			"GLU",
			"GL",
			"X11"
			--"dl",
			--"pthread",
			--"stdc++fs"
		}

		defines
		{
			"OG_PLATFORM_LINUX"
		}

	filter "system:windows"
		systemversion "latest"

		-- Add Windows-specific files
		files
		{
			"%{prj.name}/src/Platform/Windows/**.h",
			"%{prj.name}/src/Platform/Windows/**.cpp"
		}

		links
        {
            "opengl32.lib"
        }

		defines
		{
			"OG_PLATFORM_WINDOWS"
		}

	filter "configurations:Debug"
        defines "OG_DEBUG"
        runtime "Debug"
        symbols "on"

    filter "configurations:Release"
        defines "OG_RELEASE"
        runtime "Release"
        optimize "on"

    filter "configurations:Shipping"
        defines "OG_SHIPPING"
        runtime "Release"
        optimize "on"
