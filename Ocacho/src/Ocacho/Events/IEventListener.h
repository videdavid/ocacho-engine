/************************************************************************/
/* Event listener for event system - INTERFACE
*
* This interface should be implemented in all the classes that want
* to handle Ocacho events, overriding the method ::onEvent(Event&).
* 
* Each time an event occurs, the onEvent method will be called on
* each class that has implemented this interface
/************************************************************************/

#pragma once

#include "Ocacho/Events/Event.h"

namespace Ocacho
{
	class EventListener
	{
	public:
		
		/**
		* Called each time an event occurs
		* 
		* @param e The event
		*/
		virtual void onEvent(Event& e) = 0;
	};
}



