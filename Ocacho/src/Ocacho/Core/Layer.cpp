#include "ogpch.h"
#include "Layer.h"
#include "Application.h"

/*We need to define glew in case we are on linux to init glew*/
#ifdef OG_PLATFORM_LINUX
	#include <GL/glew.h>
#endif

namespace Ocacho
{
	Layer::Layer(const std::string& name /*= "Layer"*/) : m_name(name)
	{
		/*Register to the EventListener to be able to receive events*/
		Application::getInstance()->getWindow().registerEventListener(dynamic_cast<EventListener*>(this));

		/* This is temporary but we need a place to init GLEW for Linux or the program will crash  :( */
		/* Maybe we can return the Platform detection to implement a LinuxWindow and initialize Glew there */
		/* TODO: DAVID */
		/*Init GLEW*/
		#ifdef OG_PLATFORM_LINUX
			GLenum err = glewInit();
			if (err != GLEW_OK)
				exit(1);
			if (!GLEW_VERSION_2_1)
				exit(1);
		#endif
	}

	Layer::~Layer()
	{

	}
}


