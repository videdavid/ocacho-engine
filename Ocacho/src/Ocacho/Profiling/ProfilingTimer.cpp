#include "ProfilingTimer.h"
#include "Ocacho/Log/Log.h"
#include "examples/imgui_impl_glfw.h"
#include "examples/imgui_impl_opengl3.h"

namespace Ocacho
{

	ProfilingTimer::ProfilingTimer(const char *p_name, uint8_t p_outputType)
		: m_name(p_name), m_outputType(p_outputType), m_stopped(0)
	{
		m_startTimePoint = std::chrono::high_resolution_clock::now();
	}

	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------

	ProfilingTimer::~ProfilingTimer()
	{
		if (!m_stopped)
			stopTimer();
	}

	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------


	void ProfilingTimer::stopTimer()
	{
		timePoint endTimepoint = std::chrono::high_resolution_clock::now();

		auto startTime = std::chrono::time_point_cast<std::chrono::microseconds>(m_startTimePoint).time_since_epoch().count();

		auto endTime = std::chrono::time_point_cast<std::chrono::microseconds>(endTimepoint).time_since_epoch().count();

		auto diff = endTime - startTime;
		double ms = diff * 0.001;

		if (m_outputType == IMGUI_OUTPUT)
		{
			char label[50];
			strcpy(label, "%.3fms ");
			strcat(label, __FILE__);
			strcat(label, "::");
			strcat(label, m_name);
			ImGui::Text(label, ms);
		}
		else if (m_outputType == CONSOLE_OUTPUT)
		{
			OG_LOG_PROFILE("{0}ms - Function/Scope:{1}", ms, m_name);
		}

		m_stopped = 1;
	}
}
