-- Ocacho dependencies --

IncludeDir = {}
IncludeDir["GLFW"] = "%{wks.location}/Ocacho/vendor/GLFW/include"
IncludeDir["Glad"] = "%{wks.location}/Ocacho/vendor/Glad/include"
IncludeDir["ImGui"] = "%{wks.location}/Ocacho/vendor/imgui"
IncludeDir["glm"] = "%{wks.location}/Ocacho/vendor/glm"