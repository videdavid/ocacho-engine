/************************************************************************/
/* Build for ImGui.
*
* We need to build and include some files and macros to be able to use ImGui properly.
* This file does that.
/************************************************************************/

#include "ogpch.h"

#ifdef OG_PLATFORM_WINDOWS
	#define IMGUI_IMPL_OPENGL_LOADER_GLAD 
#elif defined OG_PLATFORM_LINUX
	#define IMGUI_IMPL_OPENGL_LOADER_GLEW 
#endif

#include "examples/imgui_impl_glfw.cpp"
#include "examples/imgui_impl_opengl3.cpp"