#include "ogpch.h"
#include "Application.h"
#include "Ocacho/Events/IEventListener.h"
#include "Ocacho/Events/ApplicationEvent.h"
#include "Ocacho/Profiling/ProfilingTimer.h"

//TEMPORARY
#include <glad/glad.h>

namespace Ocacho {

	Application::Application()
	{
		m_instance = this;

		m_window = std::unique_ptr<Window>(new Window);

		m_window->registerEventListener(dynamic_cast<EventListener*>(this));
		
		m_imguiLayer = new ImGuiLayer;
		
		pushOverlay(m_imguiLayer);
	}

	Application::~Application()
	{
	}

	void Application::onEvent(Event& e)
	{
		EventDispatcher dispatcher(e);

		/*Dispatching through the differents events*/
		dispatcher.dispatch<WindowCloseEvent>(OG_BIND_EVENT_FN(Application::onWindowClosed));
	}

	void Application::run()
	{
		while (m_running)
		{
			glClearColor(1, 0, 1, 1);
			glClear(GL_COLOR_BUFFER_BIT);

			for (Layer* layer : m_layerStack)
				layer->onUpdate();

			m_imguiLayer->init();

			//This is possible cause we implemented a begin and end on layerstack
			for (Layer* layer : m_layerStack)
				layer->onRender();
			
			m_imguiLayer->shutdown();

			m_window->onUpdate();
		}
	}


	bool Application::onWindowClosed(WindowCloseEvent& e)
	{
		m_running = false;
		return true;
	}

}