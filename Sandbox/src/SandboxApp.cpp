#include <Ocacho.h>

#include "imgui/imgui.h"

class ExampleLayer : public Ocacho::Layer
{
public:
	ExampleLayer() : Layer("Example")
	{

	}

	virtual void onRender() override
	{
		ImGui::Begin("Logging");

		{
			OG_PROFILE_SCOPE("TestScope",IMGUI_OUTPUT)
			float value = 0;
			for (int i=0; i < 100 ; i++)
				value += 2;
		}

		ImGui::End();

		//ImGui::End();

		//Ocacho::Input::isKeyPressed(Ocacho::Key::A);
	
		//Ocacho::Input::isMouseButtonPressed(Ocacho::Mouse::Button0);
	}

	virtual void onAttach() override
	{
	}

	//void onUpdate() override { OG_LOG_INFO("ExampleLayer::Update"); }
	//virtual void onEvent(Ocacho::Event& event) override { OG_LOG_TRACE("{0}",event); }
};


class Sandbox : public Ocacho::Application
{
public: 
	Sandbox(){	pushLayer(new ExampleLayer());}

	~Sandbox(){}

	GENERATE_CLASS_METADATA(Sandbox)
};

Ocacho::Application* Ocacho::createApplication()
{
	return new Sandbox();
}