/************************************************************************/
/* Derived Mouse events from event
*
* Events for managing all the mouse input events.
/************************************************************************/

#pragma once

#include "Event.h"

namespace Ocacho {

	/************************************************************************/
	/* Mouse Moved Event
	/************************************************************************/
	class MouseMovedEvent : public Event
	{
		PRIVATE(float, GET_INLINE, m_mouseX, X);
		PRIVATE(float, GET_INLINE, m_mouseY, Y);

	public:
		MouseMovedEvent(float x, float y): m_mouseX(x), m_mouseY(y) {}

		std::string toString() const override
		{
			std::stringstream ss;
			ss << "MouseMovedEvent: " << m_mouseX << ", " << m_mouseY;
			return ss.str();
		}

		EVENT_CLASS_TYPE(MouseMoved)
		EVENT_CLASS_CATEGORY(EventCategoryMouse | EventCategoryInput)
	};

	/************************************************************************/
	/* Mouse Scroll Event
	/************************************************************************/
	class MouseScrolledEvent : public Event
	{
		PRIVATE(float, GET_INLINE, m_offsetX, OffsetX);
		PRIVATE(float, GET_INLINE, m_offsetY, OffsetY);
	public:
		MouseScrolledEvent(float xOffset, float yOffset): m_offsetX(xOffset), m_offsetY(yOffset) {}

		std::string toString() const override
		{
			std::stringstream ss;
			ss << "MouseScrolledEvent: " << getOffsetX() << ", " << getOffsetY();
			return ss.str();
		}

		EVENT_CLASS_TYPE(MouseScrolled)
		EVENT_CLASS_CATEGORY(EventCategoryMouse | EventCategoryInput)
	};

	/************************************************************************/
	/* Base Mouse event
	*
	* This is gonna be the base Mouse event for the derived Mouse events.
	* This contains the mouse button.
	/************************************************************************/
	class MouseButtonEvent : public Event
	{
		PROTECTED(int, GET_INLINE, m_button, MouseButton);

	protected:
		MouseButtonEvent(int button)
			: m_button(button) {}

	public:
		EVENT_CLASS_CATEGORY(EventCategoryMouse | EventCategoryInput)
	};


	/************************************************************************/
	/* Mouse Button Pressed event
	/************************************************************************/
	class MouseButtonPressedEvent : public MouseButtonEvent
	{
	public:
		MouseButtonPressedEvent(int button): MouseButtonEvent(button) {}

		std::string toString() const override
		{
			std::stringstream ss;
			ss << "MouseButtonPressedEvent: " << m_button;
			return ss.str();
		}

		EVENT_CLASS_TYPE(MouseButtonPressed)
	};

	/************************************************************************/
	/* Mouse Button Released event
	/************************************************************************/
	class MouseButtonReleasedEvent : public MouseButtonEvent
	{
	public:
		MouseButtonReleasedEvent(int button): MouseButtonEvent(button) {}

		std::string toString() const override
		{
			std::stringstream ss;
			ss << "MouseButtonReleasedEvent: " << m_button;
			return ss.str();
		}

		EVENT_CLASS_TYPE(MouseButtonReleased)
	};

}