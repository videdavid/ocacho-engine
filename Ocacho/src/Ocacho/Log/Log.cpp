#include "ogpch.h"
#include "Log.h"
#include "spdlog/sinks/stdout_color_sinks.h"

namespace Ocacho {

	std::shared_ptr<spdlog::logger> Log::s_engineLogger;
	std::shared_ptr<spdlog::logger> Log::s_gameLogger;
	std::shared_ptr<spdlog::logger> Log::s_profileLogger;

	void Log::init()
	{
		/* Flag to activate extended logging (1: Activated) (0: Disabled)*/
		uint8_t extendedLogging = 1;

		s_engineLogger = spdlog::stdout_color_mt("ENGINE");
		s_engineLogger->set_level(spdlog::level::trace);

		s_gameLogger = spdlog::stdout_color_mt("GAME");
		s_gameLogger->set_level(spdlog::level::trace);

		s_profileLogger = spdlog::stdout_color_mt("PROFILING");
		s_profileLogger->set_level(spdlog::level::trace);
		s_profileLogger->set_pattern("%n: %v%$");

		if(extendedLogging)
		{
			s_engineLogger->set_pattern("[%s {%#}] %n: %v%$");
			s_gameLogger->set_pattern("[%s {%#}] %n: %v%$");
		}
		else{
			s_engineLogger->set_pattern("%n: %v%$");
			s_gameLogger->set_pattern("[%s {%#}] %n: %v%$");
		}
	}
}