/************************************************************************/
/* Log system for Ocacho engine.
* 
* This system is only for debug purposes.
* They will be disabled in the Release/Shipping version
/************************************************************************/

#pragma once

/*SPDLOG minimum level of loggingg*/
#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_TRACE

#include "Ocacho/Macros/DefinitionMacros.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"

namespace Ocacho {

	class Log
	{
		PRIVATE_STATIC(std::shared_ptr<spdlog::logger>, GET_INLINE_STATIC, s_engineLogger, EngineLogger);
		PRIVATE_STATIC(std::shared_ptr<spdlog::logger>, GET_INLINE_STATIC, s_gameLogger, GameLogger);
		PRIVATE_STATIC(std::shared_ptr<spdlog::logger>, GET_INLINE_STATIC, s_profileLogger, ProfileLogger);

	public:
		static void init();
	};
}

//TODO: This should be change by a make - premake definition 
#define OG_ENABLE_LOGGING 1

#if OG_ENABLE_LOGGING

	#define OG_LOG_PROFILE(...) 			SPDLOG_LOGGER_DEBUG(Ocacho::Log::getProfileLogger(),__VA_ARGS__)

	/*Engine log macros - This should be used by the engine*/
	#define OG_ENGINE_LOG_DEBUG(...)     	SPDLOG_LOGGER_TRACE(Ocacho::Log::getEngineLogger(),__VA_ARGS__)
	#define OG_ENGINE_LOG_INFO(...)     	SPDLOG_LOGGER_INFO(Ocacho::Log::getEngineLogger(),__VA_ARGS__)
	#define OG_ENGINE_LOG_WARN(...)     	SPDLOG_LOGGER_WARN(Ocacho::Log::getEngineLogger(),__VA_ARGS__)
	#define OG_ENGINE_LOG_ERROR(...)    	SPDLOG_LOGGER_ERROR(Ocacho::Log::getEngineLogger(),__VA_ARGS__)
	#define OG_ENGINE_LOG_CRITICAL(...)   	SPDLOG_LOGGER_CRITICAL(Ocacho::Log::getEngineLogger(),__VA_ARGS__)


	/*Game log macros - This should be used by the game application*/
	#define OG_GAME_LOG_DEBUG(...)     		SPDLOG_LOGGER_TRACE(Ocacho::Log::getGameLogger(),__VA_ARGS__)
	#define OG_GAME_LOG_INFO(...)     		SPDLOG_LOGGER_INFO(Ocacho::Log::getGameLogger(),__VA_ARGS__)
	#define OG_GAME_LOG_WARN(...)     		SPDLOG_LOGGER_WARN(Ocacho::Log::getGameLogger(),__VA_ARGS__)
	#define OG_GAME_LOG_ERROR(...)    		SPDLOG_LOGGER_ERROR(Ocacho::Log::getGameLogger(),__VA_ARGS__)
	#define OG_GAME_LOG_CRITICAL(...)   	SPDLOG_LOGGER_CRITICAL(Ocacho::Log::getGameLogger(),__VA_ARGS__)
#else
	#define OG_ENGINE_LOG_DEBUG(...)     	
	#define OG_ENGINE_LOG_INFO(...)     	
	#define OG_ENGINE_LOG_WARN(...)     	
	#define OG_ENGINE_LOG_ERROR(...)    	
	#define OG_ENGINE_LOG_CRITICAL(...)   	


	#define OG_GAME_LOG_DEBUG(...)     		
	#define OG_GAME_LOG_INFO(...)     		
	#define OG_GAME_LOG_WARN(...)     		
	#define OG_GAME_LOG_ERROR(...)    		
	#define OG_GAME_LOG_CRITICAL(...)   	
#endif