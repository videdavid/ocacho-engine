/************************************************************************/
/* This is the precompiled header used by the core engine.
* 
* This includes all the libraries from std needed.
* This includes all the Ocacho modules normally needed as Log.h or macros
* 
/************************************************************************/

#include <iostream>
#include <memory>
#include <utility>
#include <algorithm>
#include <functional>

#include <string>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#include <cmath>
#include <type_traits>

#ifdef OG_PLATFORM_WINDOWS
	#include <Windows.h>
#elif defined OG_PLATFORM_LINUX
	#include <signal.h>
#endif

/*Ocacho modules*/
#include "Ocacho/Core/Core.h"
#include "Ocacho/Log/Log.h"
#include "Ocacho/Input/Input.h"
#include "Ocacho/Input/KeyCodes.h"
#include "Ocacho/Input/MouseCodes.h"
#include "Ocacho/Macros/BaseObjectMacros.h"
#include "Ocacho/Macros/DefinitionMacros.h"
#include "Ocacho/Core/Assertion.h"
