/************************************************************************/
/* Representation and implementation of a Ocacho Window
*
* Also, window handles the events listeners to propagate the events
* through them.
/************************************************************************/

#pragma once

#include "ogpch.h"
#include "Ocacho/Events/Event.h"
#include "Ocacho/Events/IEventListener.h"

/*Fast-forward declaration because an error is thrown if we don't do this*/
struct GLFWwindow;

namespace Ocacho {

	/************************************************************************/
	/* Window Props
	*
	* Struct of the window's stats.
	* Used for create a window.
	/************************************************************************/
	struct WindowProps
	{
		std::string title;
		unsigned int width;
		unsigned int height;
		bool vSync;

		WindowProps(const std::string& p_title = "Ocacho Engine",
			        unsigned int p_width = 1280,
			        unsigned int p_height = 720,
					bool p_vSync = true)
			: title(p_title), width(p_width), height(p_height), vSync(p_vSync) {}
	};

	/************************************************************************/
	/* Window
	/************************************************************************/
	class Window
	{
	protected:
		/*Registered event listeners for events propagation*/
		std::vector<EventListener*> m_eventListeners;

		/*Struct of this window stats*/
		WindowProps m_props;

		/*Native window, in this case a GLFW window*/
		GLFWwindow* m_window;

	protected:
		/**
		* Initialize the window
		*/
		virtual void init();

		/**
		* Set all the GLFW callbacks.
		* For each GLFW event we create an Ocacho event.
		* Then we propagate it through the listeners each time an event occurs.
		*/
		virtual void initCallbacks();

		/**
		* Shutdown the window
		*/
		virtual void shutdown();

		/**
		* Whether an event listener is contained in the event listeners vector or not
		*/
		bool isEventListenerContained(EventListener* e);

		/**
		* Loop through all the registered event listeners propagating the event e
		* 
		* @param e The event we want to propagate to the listeners
		*/
		void callOnEventOnListeners(Event& e);

	public:

		Window(const WindowProps& props = WindowProps());
		~Window();

		/**
		* Updating the window, called each frame
		*/
		virtual void onUpdate();

		/**
		* Get the native window of window. In this case, a GLFWwindow*
		*/
		virtual GLFWwindow* getNativeWindow() const { return m_window; }

		/**
		* Register (add to the vector) an event listener to be able to receive events.
		* 
		* @return Was it successful?
		*/
		bool registerEventListener(EventListener* e);

		/**
		* Unregister (remove from the vector) an event listener to disable receiving events.
		*
		* @return Was it successful?
		*/
		bool unregisterEventListener(EventListener* e);

		/************************************************************************/
		/* Window props getters and setter
		/************************************************************************/

		/**
		* Get the title of this window
		*/
		virtual inline std::string getTitle() const { return m_props.title; }

		/**
		* Get the width of this window
		*/
		virtual inline unsigned int getWidth() const { return m_props.height; }

		/**
		* Get the height of this window
		*/
		virtual inline unsigned int getHeight() const { return m_props.height; }

		/**
		* Change the status of the window's vsync
		*/
		virtual void setVSync(bool enabled);

		/**
		* Get the status of the window's vsync
		*/
		virtual inline bool isVSync() const { return m_props.vSync; }
	};

}