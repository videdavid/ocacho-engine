# Ocacho
Ocacho Engine

Start by cloning the repository with git clone --recursive https://gitlab.com/videdavid/ocacho-engine.

If the repository was cloned non-recursively previously, use git submodule update --init to clone the necessary submodules.

In case submodules are failing --> git submodule update --force --recursive --init --remote

Setup linux:

Scripts/Linux/Setup to install premake5
Scripts/Linux/Build_Run to compile and execute

Setup windows:

Scripts/Windows/Build_Run.bat   !!!Necessary Visual studio community 2019
